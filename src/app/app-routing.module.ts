import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ActorsComponent } from './components/actors/actors.component'
import { EditActorComponent } from './components/edit-actor/edit-actor.component'
import { FourohfourComponent } from './components/fourohfour/fourohfour.component'

const routes: Routes = [
  { path: 'actors', component: ActorsComponent },
  { path: 'edit_actor', component: EditActorComponent },
  { path: 'edit_actor/:id', component: EditActorComponent},
  { path: '', component: ActorsComponent},
  { path: 'not-found', component: FourohfourComponent},
  { path: '**', redirectTo: 'not-found'}



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
