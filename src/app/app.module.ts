import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { ActorsService } from './services/actors.service'
import { ServerService } from './services/server.service'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ActorsComponent } from './components/actors/actors.component';
import { EditActorComponent } from './components/edit-actor/edit-actor.component';
import { FourohfourComponent } from './components/fourohfour/fourohfour.component';

@NgModule({
  declarations: [
    AppComponent,
    ActorsComponent,
    EditActorComponent,
    FourohfourComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    ActorsService,
    ServerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
