import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class ServerService {
  private BASE_URL: string = 'http://localhost:8000/api/';

  constructor(private http: HttpClient) 
  {}

  public get<T>(url: string): Observable<any> 
  {
    return this.http.get(this.BASE_URL + url);
  }

  public post<T>(url: string, body: T): Observable<any>
  {
    // TODO add this as std header later on
    const header = new HttpHeaders({ 'Content-Type': 'application/json',
    Authorization: 'my-auth-token' });
    return this.http.post(this.BASE_URL + url, body, { headers: header });
  }
}
