import { Injectable } from '@angular/core';
import { ServerService } from './server.service';
import { Actor } from '../Models/actor.model';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ActorsService {

  constructor(private server: ServerService) 
  {}

  public getAll(): Observable<Actor[]> 
  {
    return this.server.get<Actor[]>('movies/actors/').pipe( //le pipe sert a filter la response et le transformer en Actor
      map(res => res.map((m: any) => new Actor(m))),    // on parcours chaque element du tableau et on remplace
      catchError(err => 
        {
          console.error(err);
          return [];
        })
    );
  }

  public getOneByID(id: number): Observable<Actor> 
  {
    return this.server.get<Actor>('movies/actors/'+id).pipe( 
      map(res => res.legth > 0 ? new Actor(res[0]): new Actor({})),    
      catchError(err => 
        {
          console.error(err);
          return [];
        })
    );
  }



  public addActor(actor: Actor):  Observable<Actor[]> 
  {
    return this.server.post<Actor>('movies/actors/create', actor).pipe(
      map(res => res.map((m: any) => new Actor(m))),
      catchError(err => 
        {
          console.error(err);
          return [];
        })
    );
  }
}
