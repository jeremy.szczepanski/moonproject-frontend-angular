import { Component, OnInit } from '@angular/core';
import { inject } from '@angular/core/testing';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, ValidatorFn } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Actor } from 'src/app/Models/actor.model';
import { ActorsService } from '../../services/actors.service'

@Component({
  selector: 'app-edit-actor',
  templateUrl: './edit-actor.component.html',
  styleUrls: ['./edit-actor.component.scss']
})
export class EditActorComponent implements OnInit {
  actorForm!: FormGroup;
  firstnameCtl!: FormControl;
  lastnameCtl!: FormControl;

  constructor(private actorService: ActorsService, private router: Router,private route: ActivatedRoute,  private formBuilder: FormBuilder) 
  
  {
    this.initForm();
  }

  ngOnInit(): void {
    if(this.route.snapshot.params["id"])
    {
      const actor= this.actorService.getOneByID(this.route.snapshot.params["id"]).subscribe(m =>
        {
          const actor = m;
          console.log(actor);
        }
        );
      
    }
  }

  initForm(): void
  {
    this.firstnameCtl = this.formBuilder.control('', [Validators.required]);
    this.lastnameCtl = this.formBuilder.control('', [Validators.required]);

    this.actorForm = this.formBuilder.group({
      first_name: this.firstnameCtl,
      last_name: this.lastnameCtl
    });
  }

  onSubmit()
  {
    const formVal = this.actorForm.value;
    formVal.id = 0;
    
    console.log("first_name : " + formVal.first_name + " last_name : " + formVal.last_name);
    const newActor = new Actor(formVal);
    console.log(newActor);
    this.actorService.addActor(newActor).subscribe(m => {}); //toutes les requetes get post put il faut subscribe
    this.router.navigate(['/actors']);
  }

}
